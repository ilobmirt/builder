#!/bin/bash

[[ ! ${tools} ]] && export tools=tools

. ${tools}/common

#Extract various sources to a designated folder
function do_extract(){

    local src=''
    local dest=''
    local what_type=''

    #Param and Flag Setter
    while [[ $# -gt 0 ]] ; do
        if [[ $# -gt 0 ]] ; then
            opt_flag="${1}"
        else
            opt_flag=''
        fi
        if [[ $# -gt 1 ]] ; then
            opt_param="${2}"
        else
            opt_param=''
        fi

        if [[ "${opt_flag:0:1}" == "-" ]] ; then
            opt_shift=2
            case "${opt_flag}" in
                '-i' | '--input')
                    src="${opt_param}"
                ;;
                '-o' | '--output')
                    dest="$(echo ${opt_param} | sed 's/\\/\//g;s/\/*$//;s/$/\//')"
                ;;
                '-v' | '--verbose')
                    IS_EXTRACT_VERBOSE=yes
                ;;
                *)
                    [[ ${opt_flag} ]] || continue
                    echo "$0 error: invalid option \"${opt_flag}\"" |errorlog
				    exit 1
            esac
        else
            opt_shift=1
        fi

        shift ${opt_shift}
    done

    what_type="$(check_source_type -n -p ${src})"

    if [ ! -z "${IS_EXTRACT_VERBOSE+x}" ]; then
        echo "media-packer >> do_extract >> dest[${dest}] type[${what_type}]"
    fi

    case "${what_type}" in
        'folder')
            src="$(echo ${src} | sed 's/\\/\//g;s/\/*$//;s/$/\//')"
            if [[ "${src}" != "${dest}" ]] ; then
                cp -a ${src} ${dest}
            fi
        ;;
        'zip')
            unzip ${src} -d ${dest}
        ;;
        'iso' )
            xorriso -osirrox on -indev ${src} -extract / ${dest}
        ;;
        'tar')
            tar -xf ${src} -C ${dest}
        ;;
        'fat')
            mcopy -smn -D o -i ${src} ::/ $( echo ${dest} | sed 's/\\/\//g;s/\/*$//;s/$/\//' )
        ;;
        *)
            cp -a ${src} ${dest}
        ;;
    esac

    if [ ! -z "${IS_EXTRACT_VERBOSE+x}" ]; then
        echo "media-packer >> do_extract >> file contents[${dest}]"
        ls -hal ${dest}
    fi

    unset IS_EXTRACT_VERBOSE

}

#Compress folder contents to various target media
function do_compress(){

    local src=''
    local dest=''
    local what_type=''

    #Param and Flag setter
    while [[ $# -gt 0 ]] ; do
        if [[ $# -gt 0 ]] ; then
            opt_flag="${1}"
        else
            opt_flag=''
        fi
        if [[ $# -gt 1 ]] ; then
            opt_param="${2}"
        else
            opt_param=''
        fi

        if [[ "${opt_flag:0:1}" == "-" ]] ; then
            opt_shift=2
            case "${opt_flag}" in
                '-i' | '--input')
                    src="$(echo ${opt_param} | sed 's/\\/\//g;s/\/*$//;s/$/\//')"
                ;;
                '-o' | '--output')
                    dest="${opt_param}"
                ;;
                '-v' | '--verbose')
                    IS_COMPRESS_VERBOSE=yes
                ;;
                *)
                    [[ ${opt_flag} ]] || continue
                    echo "$0 error: invalid option \"${opt_flag}\"" |errorlog
				    exit 1
            esac
        else
            opt_shift=1
        fi

        shift ${opt_shift}
    done

    what_type="$(check_source_type -n -p ${dest})"

    if [ ! -z "${IS_COMPRESS_VERBOSE+x}" ] ; then
        echo "media-packer >> do_compress >> file contents[${src}]"
        ls -hal ${src}
        echo "media-packer >> do_compress >> dest[${dest}] type[${what_type}]"
    fi

    case "${what_type}" in
        'folder')
            dest="$(echo ${dest} | sed 's/\\/\//g;s/\/*$//;s/$/\//')"
            if [[ "${src}" != "${dest}" ]] ; then
                cp -a ${src} ${dest}
            fi
        ;;
        'zip')
            zip -r ${dest} ${src}
        ;;
        'tar')
            tar -cf ${dest} ${src}
        ;;
        'iso')
            xorriso -outdev ${dest} -blank as_needed -joliet on -map ${src} /
        ;;
        'fat')
            mcopy -sm -D o -i ${dest} $( echo ${src} | sed 's/\\/\//g;s/\/*$//;s/$/\/*/' ) ::/
        ;;
        *)
        ;;
    esac

    unset IS_COMPRESS_VERBOSE

}

function check_source_type(){

    local return_type=''
    local check_target=''
    local check_extension=''

    #By default, check if the source already exists
    DO_CHECK=yes

    while [[ $# -gt 0 ]] ; do
	    opt_flag="${1}"
        if [[ "${opt_flag:0:1}" != "-" ]] ; then
            check_target=${opt_flag}
            opt_flag=''
        fi

        case "${opt_flag}" in
            '')
                #Relax, do nothing
            ;;
            '-n' | '--no-check')
                unset DO_CHECK
            ;;
            '-p' | '--precise')
                #When specified, be more specific about the type of target in our results
                IS_PRECISE=yes
            ;;
            *)
                [[ ${opt_flag} ]] || continue
                echo "$0 error: invalid option \"${opt_flag}\"" |errorlog
				exit 1
            ;;
        esac

        shift
    done

    if ( [ -f "${check_target}" ] ) || ( [[ "*$(echo ${check_target} | sed 's/.*\///g;s/[^\.]//g')*" != "**" ]] && [ -z "${DO_CHECK+x}" ] ) ; then
        return_type='file'
        check_extension="${check_target##*.}"
    elif ( [ -d "${check_target}" ] ) || ( [ -z "${DO_CHECK+x}" ]) ; then
        return_type='folder'
    else
        return_type='invalid'
    fi

    case "${check_extension}" in
        'iso' | 'img' | 'fat')
            if [ -z "${IS_PRECISE+x}" ] ; then
                return_type='disk'
            else
                # Let's check back here
                return_type="$(check_img_format "${check_target}")"
            fi
        ;;
        'zip')
            return_type='zip'
        ;;
        'bz2' | 'gz' | 'tar')
            return_type='tar'
        ;;
        '')
            #Relax, do nothing
        ;;
        *)
            if [ -z "${IS_PRECISE+x}" ] ; then
                return_type='file'
            else
                return_type=${check_extension}
            fi
    esac

    echo ${return_type}
    unset DO_CHECK
    unset IS_PRECISE

}

function check_img_format(){

    local return_type=''
    local check_type=''
    local file_target="${1}"

    check_type="$(file -sb "${file_target}" | sed 's|Linux\ rev\ [0-9]*\.[0-9]*||;s|\ .*||;s|\/.*||')"

    case "${check_type}" in
        'DOS')
            return_type='fat'
        ;;
        'ISO')
            return_type='iso'
        ;;
        *)
            return_type=${check_type}
    esac

    echo ${return_type}
}

function main(){

    local in_target=''
    local in_folder=''
    local in_type=''
    local out_target=''
    local out_folder=''
    local out_type=''

    local opt_flag=''
    local opt_param=''
    local opt_shift=1

    #IS_VERBOSE=yes

    while [[ $# -gt 0 ]] ; do
        if [[ $# -gt 0 ]] ; then
            opt_flag="${1}"
        else
            opt_flag=''
        fi
        if [[ $# -gt 1 ]] ; then
            opt_param="${2}"
        else
            opt_param=''
        fi

        if [[ "${opt_flag:0:1}" == "-" ]] ; then
            opt_shift=2
            case "${opt_flag}" in
                '-i' | '--input')
                    IS_INPUT=yes
                ;;
                '-o' | '--output')
                    IS_OUTPUT=yes
                ;;
                '-v' | '--verbose')
                    IS_VERBOSE=yes
                ;;
                *)
                    [[ ${opt_flag} ]] || continue
                    echo "$0 error: invalid option \"${opt_flag}\"" |errorlog
				    exit 1
            esac
        else
            opt_shift=1
            opt_param=${opt_flag}
            if [[ "${in_target}" == "" ]] ; then
                IS_INPUT=yes
            else
                IS_OUTPUT=yes
            fi
        fi

        if [[ ${IS_INPUT} ]]; then
            in_target=${opt_param}
            in_type="$(check_source_type -n ${opt_param})"
            if [ ! -z "${IS_VERBOSE+x}" ] ; then
                echo "[${in_target}] in_type=${in_type}"
            fi
            if [[ "${in_type}" == "folder" ]] ; then
                in_folder="${in_target}"
            else
                in_folder="/tmp/$(rand_string_gen 8)/"
            fi
            if [ ! -z "${IS_VERBOSE+x}" ]; then
                echo "in_folder=${in_folder}"
            fi
            unset IS_INPUT
        fi

        if [[ ${IS_OUTPUT} ]]; then
            out_target=${opt_param}
            out_type="$(check_source_type -n ${opt_param})"
            if [ ! -z "${IS_VERBOSE+x}" ]; then
                echo "[${out_target}] out_type=${out_type}"
            fi
            if [[ "${out_type}" == "folder" ]] ; then
                out_folder="${out_target}"
            else
                out_folder="/tmp/$(rand_string_gen 8)/"
            fi
            if [ ! -z "${IS_VERBOSE+x}" ] ; then
                echo "out_folder=${out_folder}"
            fi
            unset IS_OUTPUT
        fi

        case "$(check_source_type -n ${opt_param})" in
            'disk')
                HAS_DISK=yes
            ;;
            'invalid')
                echo "$0 error: \"${opt_param}\" is not a valid target" |errorlog
		        exit 1
            ;;
        esac

        shift ${opt_shift}
    done

    #Did we set one of them to a disk?
    if [ -z "${HAS_DISK+x}" ] ; then
        echo "$0 error: Disk file is not supplied" |errorlog
		exit 1
    else
        unset HAS_DISK
    fi

    # If we do have verbosity set - pass it to the other functions
    if [ -z "${IS_VERBOSE+x}" ] ; then
        verbosity_option=''
    else
        verbosity_option='-v'
    fi

    #Pushing from disk to target
    if [[ "${in_type}" == "disk" ]] ; then

        if [[ "${out_target}" != "${out_folder}" ]] ; then
            mkdir -p ${out_folder}
        fi

        do_extract ${verbosity_option} -i ${in_target} -o ${out_folder}
        do_compress ${verbosity_option} -i ${out_folder} -o ${out_target}

        if [[ "${out_target}" != "${out_folder}" ]] ; then
            rm -rf ${out_folder}
        fi

    #Pulling from target to disk
    elif [[ "${out_type}" == "disk" ]] ; then

        if [[ "${in_target}" != "${in_folder}" ]] ; then
            mkdir -p ${in_folder}
            do_extract ${verbosity_option} -i ${in_target} -o ${in_folder}
        fi

        do_compress ${verbosity_option} -i ${in_folder} -o ${out_target}

        if [[ "${in_target}" != "${in_folder}" ]] ; then
            rm -rf ${in_folder}
        fi
    fi

    unset IS_VERBOSE

}

main $@