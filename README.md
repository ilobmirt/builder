# FreeDOS Release and Installation Media Builder

Automation tools for creating a [FreeDOS](http://www.freedos.org) release and
it's associated install media.

It is designed to be used with [Podman](https://podman.io/) or [Docker](https://www.docker.com/)
with a base image of [opensuse/tumbleweed:latest](https://hub.docker.com/r/opensuse/tumbleweed).

_Other methods for creating a release may work. But, they are not officially
supported and will most likely create security issues on the building host.
Therefore, other methods are discouraged. This is not end-user software. It is
strictly a build environment/compiler for the end user software (aka FreeDOS).
The host OS used to build the release is not relevant. It could change at any
point to continue to provide a free and easy release build platform for the
operating system installation media._

***
### Install the FreeDOS builder tools.

	sudo ./install

This will automatically apply any updates to all required system packages and
download any additionally software needed to build a FreeDOS release.

***
### Create the FreeDOS media.

	sudo make all

This will create the latest FREEDOS media images. After the images are made, they will end up
being pushed to ${OUTPUT_DIR} where there should be volume or folder connected to the 
container running this software

***
### Build the Podman Image

	podman build --tag builder:latest -f ./Containerfile

This will create the latest image of this project for Podman/Docker. After creation, running
containers based off these images will generate FREEDOS media. 

***
### Import the FreeDOS builder image

You can pull a ready to use container for this project

	podman pull docker.io/ilobmirt/builder:latest

or

	podman pull registry.gitlab.com/ilobmirt/builder:latest

***
### Using the image

With the builder image, you can generate the latest freedos install media locally

**Output to a volume:**

	podman run --volume FreeDOS_Media:/output -it docker.io/ilobmirt/builder:latest

**Output to a local folder:**

	podman run --security-opt label=disable --volume ./FreeDOS_Media:/output -it builder:latest
