#!/bin/bash

[[ ! ${tools} ]] && export tools=tools

. ${tools}/common

if [[ -f ${temp}/fd-x86.img ]] ; then
	rm -rf ${temp}/fd-x86.img
	if [[ -f ${cache}/fd-x86.img ]] ; then
		rm -f ${cache}/fd-x86.img
	fi
fi

test -f ${cache}/fd-x86.img && exit 0

new_release_needed

function make_fd_x86_img () {

	echo "Make bootable single disk image of Floppy Only Edition as '${cfg_prefix}-HDx86'"

 	cleandir ${temp}/install || return $?

	unzip -o -L -q "${cache}/bin-x86.zip" -d "${temp}/install" || return 1

	mkdir -p $(isVerbose) "${temp}/install/SLICED" || return 1

	cp -fa $(isVerbose) "${cache}/slices"/* "${temp}/install/SLICED" || return 1

	cat ${temp}/install/fdauto.bat | sed -e 's/SETUP.BAT BOOT/SETUP.BAT BOOT ADV USB/g'>${temp}/fdauto.bat || return 1

	mv -f ${temp}/fdauto.bat ${temp}/install/fdauto.bat || return 1

	# two methods to calculate size, I haven't decided yet
	local req=0
	local s
	if [[ 1 -eq 2 ]] ; then
		# calculate required space needed for image, this probably ain't perfect
		local f c x
		local alu=4096
		while IFS=''; read -r f ; do
			if [[ -d "${f}" ]] ; then
				req=$(( $req + $alu ))
			else
				s=$(stat -c %s "${f}" 2>/dev/null || echo -1)
				if [[ $s -eq -1 ]] ; then
					echo "error: reading size of '${f}'" | errorlog
					return 1
				fi
				x=$(( $s / $alu * $alu ))
				[[ $x -ne $s ]] && req=$(( $req + $x + $alu )) || req=$(( $req + $s ))
			fi
		done<<<$(find "${temp}/install")
		req=$(( req / 1024 + 1 ))
	else
		local req=$(du -s "${temp}/install" | cut -d ' ' -f 1 | cut -d '	' -f 1 )
	fi

	# round up to nearest 4mb
	s=$(( ${req} / 4096 * 4096 ))
	[[ $req -ne $s ]] && s=$(( $s + 4096 ))

	echo "Requires ${s}k disk image for ${req}k of files"

	temp_drive_dir="/tmp/$(rand_string_gen 8)"
	mkdir --parents "${temp_drive_dir}/"

	cp -fa $(isVerbose) ${temp}/install/* ${temp_drive_dir}/ || ret=$?

	# remove duplicate root directory copy of COMMAND.COM
	local OLD_COM=$(fileCase -a "${temp_drive_dir}/command.com")
	if [[ -e "${OLD_COM}" ]] ; then
		rm -f "${OLD_COM}" || ret=$?
	fi

	container_make_blank_hd drive.img ${s}K || return $?
	${tools}/media-packer -i "${temp_drive_dir}" -o "${temp}/drive.img"
	rm --recursive "${temp_drive_dir}/"
	[[ $ret -ne 0 ]] && return $ret

	rm -rf ${temp}/install || return 1
	if [[ -e ${temp}/boot.img ]] ; then
		rm -rf ${temp}/boot.img || return 1
	fi

	return 0

}

make_fd_x86_img || die

mv -f $(isVerbose) ${temp}/drive.img ${cache}/fd-x86.img || die

exit $?
