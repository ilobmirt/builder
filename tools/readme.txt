### Index of files in the tools directory

elapsed				measure the elapsed execution time of a process.
serverip				return ip address of the RBE server

cache-blank-floppy	creates blank floppy images

cache-cdrom			retrieves latest package ISO from official repository
cache-fd-nls			downloads current package translations from github
cache-fdi			downloads primary installer
cache-fdi-x86		downloads secondary "floppy only" installer

cache-packages		consolidates packages
cache-matedata		creates package info and list files
cache-lists			expands package lists

cache-src-fdi		creates FDI sources package
cache-bin-fdi		creates executable version of FDI installer for media
cache-bin-x86		creates executable version of FDI-x86 installer for media

cache-dostasker		creates bootable dos task disk for vm jobs

