include settings

ifeq ($(strip $(OUTPUT_DIR)),)
OUTPUT_DIR=/output
endif

.PHONY : all banner fresh clean prerequisite prep boot_x86 media_cd media_usb media_x86 media_all release report

all : prep media_all release

banner :
	@echo
	@echo "Release Build Environment (RBE), 3rd Edition"
	@echo "MIT License"
	@echo "Copyright (C) 2021-2023 Jerome Shidel"
	@echo
	@sleep 1
	@#
	@# Version 1 ran under FreeDOS
	@# Version 2 ran mostly under Linux
	@# Current Version 3 nearly complete rewrite.

make_output :

	mkdir -p ${OUTPUT_DIR}

${cache} :

	mkdir -p ${cache}
	${tools}/cache-rbe

${temp} :

	mkdir -p ${temp}
	mkdir -p ${temp}/threads

${cdrom_iso} : ${cache} ${temp}

	${tools}/cache-cdrom

${fd_nls} : ${cache} ${temp}

	${tools}/cache-fd-nls
	
${fdi} : ${cache} ${temp} ${fd_nls}

	${tools}/cache-fdi

${fdi_x86} : ${cache} ${temp} ${fd_nls}

	${tools}/cache-fdi-x86

${cache}/groups : ${cdrom_iso} ${fd_nls}

	${tools}/cache-groups

${cache}/src-fdi.zip : ${cache} ${temp} ${fdi} ${fdi_x86} ${fdi_nls}

	${tools}/cache-src-fdi

${cache}/git-pkgs : ${cache} ${temp} ${fdi} ${fdi_x86}

	${tools}/cache-git-pkgs

${cache}/fdinst.zip : ${cache} ${temp} ${cdrom_iso} ${cache}/git-pkgs

	${tools}/cache-fdinst

${packages} : ${cache} ${temp} ${cdrom_iso} ${cache}/src-fdi.zip ${cache}/fdinst.zip

	${tools}/cache-packages

${cache}/changes.log : ${cache} ${cache}/git-pkgs ${fdi} ${fdi_x86} ${fd_nls}

	${tools}/make-changes

${cache}/metaflag : ${cache} ${temp} ${cache}/git-pkgs ${packages} ${cache}/changes.log

	${tools}/cache-metadata

${metadata} : ${cache}/metaflag

${lists} : ${cache} ${temp} ${packages} ${fdi} ${fdi_x86} ${cache}/fdinst.zip

	${tools}/cache-lists

${cache}/requirements : ${lists}

	${tools}/cache-require

${cache}/dostasker.img : ${cache} ${temp} ${cdrom_iso} ${packages} ${fdi}

	${tools}/cache-dostasker

${cache}/bin-fdi.zip : ${cache} ${temp} ${fdi} ${lists} ${fd_nls} ${cache}/requirements

	${tools}/cache-bin-fdi

${cache}/bin-x86.zip : ${cache} ${temp} ${fdi} ${fdi_x86} ${lists} ${fd_nls} ${cache}/dostasker.img ${cache}/requirements

	${tools}/cache-bin-x86

prerequisite : banner make_output ${cache} ${temp} ${cdrom_iso} ${cache}/src-fdi.zip ${cache}/bin-fdi.zip ${cache}/bin-x86.zip ${packages} ${metadata} ${lists} ${cache}/dostasker.img ${cache}/groups ${cache}/requirements

prep : prerequisite

${cache}/fd-x86.img : prerequisite ${cache}/bin-x86.zip ${cache}/slices

	${tools}/make-fd-x86-img

${cache}/fd-floppy.img : prerequisite ${cache}/dostasker.img

	${tools}/make-fd-floppy

${cache}/boot-legacy.img : prerequisite ${cache}/fd-floppy.img

	${tools}/make-boot-legacy

${cache}/boot-standard.img : prerequisite ${cache}/boot-legacy.img

	${tools}/make-boot-standard

${cache}/boot-hydra.img : prerequisite ${cache}/dostasker.img

	${tools}/make-boot-hydra

${cache}/fd-lite.img : prerequisite ${cache}/slices

	${tools}/make-fd-lite

${cache}/fd-bonus.iso : prerequisite ${cache}/slices

	${tools}/make-fd-bonus

${cache}/fd-full.img : prerequisite ${cache}/fd-lite.img ${cache}/slices

	${tools}/make-fd-full

${cache}/fd-legacy.iso : prerequisite ${cache}/boot-legacy.img ${cache}/slices

	${tools}/make-fd-legacy

${cache}/fd-hydra.iso : prerequisite ${cache}/boot-standard.img ${cache}/boot-hydra.img ${cache}/fd-x86.img

	${tools}/make-fd-hydra

${cache}/slices : prerequisite

	${tools}/make-slices

${cache}/fd-x86 : prerequisite ${cache}/bin-x86.zip ${cache}/slices

	${tools}/make-fd-x86

media_cd : ${cache}/fd-floppy.img ${cache}/fd-legacy.iso ${cache}/fd-hydra.iso ${cache}/fd-bonus.iso

media_usb : ${cache}/fd-lite.img ${cache}/fd-full.img ${cache}/boot-hydra.img

media_x86 : ${cache}/fd-x86

media_all : media_cd media_usb media_x86

${cache}/release :

	${tools}/make-release
	${tools}/report
	@test -f ${temp}/error.log && cp -f ${temp}/error.log ${OUTPUT_DIR}/error.log || rm -rf ${OUTPUT_DIR}/error.*
	@echo
	@echo Release files can be downloaded from the RBE webserver.
	@${tools}/serverip

release : ${cache}/release

report : ${cache}/release

	@test -f ${OUTPUT_DIR}/report.html || ${tools}/report

unmount :

	${tools}/mnt -qu ${cdrom_iso} boot floppy builder drive

fresh : unmount

	rm -rf ${temp}
	rm -rf ${cache}/*.zip ${cache}/*.iso ${cache}/*.img
	rm -rf ${cache}/lists ${cache}/groups ${cache}/slices
	rm -rf ${cache}/fdnpkg.cfg ${cache}/requirements
	rm -rf ${cache}/fd-x86 ${cache}/fdi*
	rm -rf ${cache}/release ${cache}/git-pkgs ${cache}/metaflag
	rm -rf ${cache}/fd-nls

clean_output : fresh

	rm -rf	${OUTPUT_DIR}/*.md ${OUTPUT_DIR}/*.doc ${OUTPUT_DIR}/*.txt ${OUTPUT_DIR}/*.log ${OUTPUT_DIR}/*.zip

clean : fresh

	rm -rf ${cache}

