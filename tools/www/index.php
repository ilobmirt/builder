<?php

function humanSize ( $bytes, array $options=null ) {

	$defaults = [
		'decimalSeparator' => '.',
		'thousandsSeparator' => ',',
		'labelSeparator' => ' ',
		'decimalPlaces' => 1,
		'firstStep' => 5120,
		'steps'	=> 1024,
		'maximum' => 8,
		'suffix' => 'iB',
		'byte' => 'byte',
		'bytes' => 'bytes',
		'labels' => ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y']
	];

    if ($options != null) {
        $defaults = array_replace_recursive($defaults, $options);
	}

	if ( $defaults['steps'] == 1000 ) {
		if ( $defaults['suffix'] == 'iB' )
			$defaults['suffix']='B';
	}

	$index=0;
	$step=$defaults['firstStep'];
	while ( $bytes >= $step ) {
		if ( $index >= $defaults['maximum'] ) break;
		$step=$defaults['steps'];
		$bytes/=$step;
		$index++;
		$label=$defaults['labels'][$index];
	}

	if ( $index == 0 ) {
		$label=($index == 1) ? $defaults['byte'] : $defaults['bytes'];
	} else {
		$label.=$defaults['suffix'];
	}

	$decimalPlaces=($bytes < $defaults['steps']) ? $defaults['decimalPlaces'] : 0 ;
	return (number_format($bytes,$decimalPlaces,$defaults['decimalSeparator'],$defaults['thousandsSeparator']) . $defaults['labelSeparator'] . $label );
}

function filestamp ( $timestamp ) {
	return date ('Y-m-d @ H:i:s', $timestamp);
}

function printHTMLPage () {

	if ( $_SERVER['SERVER_ADDR'] = $_SERVER['SERVER_NAME'] ) {
		$machine=gethostname();
	} else {
		$machine=$_SERVER['SERVER_NAME'];
	}

	$files = scandir('.');

	header('Content-Type: text/html');

	echo '<!DOCTYPE html>' . PHP_EOL;
	echo '<html DIR=ltr>' . PHP_EOL;


	echo '	<head>' . PHP_EOL;

	echo '		<title>' . $machine . ' - downloads</title>' . PHP_EOL;
	echo '		<meta timezone="' . date_default_timezone_get() . '">' . PHP_EOL;
	echo '		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">' . PHP_EOL;
	echo '		<link href="index.php?css=' . filemtime('index.php') . '" rel="stylesheet" type="text/css"/>' . PHP_EOL;
	if ( file_exists("default.css") ) echo '		<link href="default.css?' . filemtime('default.css') . '" rel="stylesheet" type="text/css"/>' . PHP_EOL;
	if ( file_exists("style.css") ) echo '		<link href="style.css?' . filemtime('style.css') . '" rel="stylesheet" type="text/css"/>' . PHP_EOL;
	if ( file_exists("index.css") ) echo '		<link href="index.css?' . filemtime('index.css') . '" rel="stylesheet" type="text/css"/>' . PHP_EOL;
	if ( file_exists("favicon.ico") ) echo '		<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>' . PHP_EOL;
	if ( file_exists("favicon.jpg") ) echo '		<link href="favicon.jpg" rel="apple-touch-icon"/>' . PHP_EOL;

	echo '	</head>' . PHP_EOL;
	echo '	<body id="webpage" >' . PHP_EOL;
	echo '		<div id="downloads">' . PHP_EOL;
	echo '			<span id="machine">' . $machine . '</span><span id="general"> downloads:</span>' . PHP_EOL;
	echo '		</div>' . PHP_EOL;
	echo '		<div id="files">' . PHP_EOL;

	$index=0;
	foreach ( $files as $key => $value  ) {

		$ext = strtolower(pathinfo($value)[extension]);
		if ( $value == 'index.html' ) continue;
		switch ($ext) {
			case 'iso':
			case 'img':
			case 'zip':
			case 'gz':
			case 'bz':
			case 'tbz':
			case 'tgz':
			case 'txt':
			case 'log':
			case 'md':
			case 'html':
			case 'doc':
				if ( $index == 0 ) {
					echo '			<ul id="filelist">' . PHP_EOL;
				}
				echo '				<li id="fileitem"><span id="filename"><a href=' . $value . '>' . ${value} . '</a></span>';
				echo '<span id="filedata"><span id="filenote">(' ;
				echo '<span id="filesize">' . humanSize(filesize($value), ['labelSeparator' => '</span> <span id="filebase">']) . '</span>';
				echo '<span id="filetext">, </span>' ;
				echo '<span id="filedate">' .  str_replace ('@', '</span><span id="filetext"> at </span><span id="filetime">', filestamp(filectime($value))) . '</span>)</span></span>' . PHP_EOL;
				$index++;
				break;
			default:
				break;
  		}
	}

	if ( $index == 0 ) {
		echo '<span id="nofiles">There no files available to download at this time.</span><span id="nofiles"></span>'. PHP_EOL;
	} else {
		echo '			</ul' . PHP_EOL;
	}

	echo '		</div>' . PHP_EOL;
	echo '		<div id="status">' . PHP_EOL;
	echo '			<span id="freespace"><span id="freesize">' . humanSize(disk_free_space('/'), ['labelSeparator' => '</span> <span id="freebase">']) . ' </span><span id="freetext"> of free space.</span></span> ' . PHP_EOL;
	echo '			<span id="statustext">Last updated on <span id="statusdate">' . str_replace ('@', '</span>at<span id="statustime">', filestamp(filemtime('.'))) . '</span>.</span> ' . PHP_EOL;
	echo '		</div>' . PHP_EOL;
	echo '	</body>' . PHP_EOL;
	echo '</html>' . PHP_EOL;

	unset ($files);
	unset ($value);
	unset ($latest_size);
	unset ($latest_time);
	unset ($latest_mul);
	unset ($machine);

}

function printCSSPage() {
	if ( $_SERVER['SERVER_ADDR'] = $_SERVER['SERVER_NAME'] ) {
		$machine=gethostname();
	} else {
		$machine=$_SERVER['SERVER_NAME'];
	}

	header('Content-Type: text/css');

	echo '/* ' . $machine . ' Stylesheet ' . filemtime('.') . ' */' . PHP_EOL;
	echo PHP_EOL;
	echo 'body { ' . PHP_EOL;
	echo '	background-color: #EEE; ' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo PHP_EOL;
	echo 'a:link { color:#0000BB; }' . PHP_EOL;
	echo 'a:visited { color:#000044; }' . PHP_EOL;
	echo 'a:hover { color:#FF0000; }' . PHP_EOL;
	echo 'a:active { color:#FF0000; }' . PHP_EOL;
	echo PHP_EOL;
	echo '#downloads {' . PHP_EOL;
	echo '	font-size: x-large;' . PHP_EOL;
	echo '	font-weight: bolder;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo PHP_EOL;
	echo '#files {' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filelist {' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#fileitem {' . PHP_EOL;
	echo '	margin: 2pt 1pt 2pt 30pt;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filedata {' . PHP_EOL;
	echo '	padding-left: 5pt;' . PHP_EOL;
	echo '	font-size: small;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filenote {' . PHP_EOL;
	echo '	font-style: italic;' . PHP_EOL;
	echo '	font-weight: bolder;' . PHP_EOL;
	echo '	color: #000000;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filetext {' . PHP_EOL;
	echo '	color: #777777;' . PHP_EOL;
	echo '	font-weight: normal;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filename {' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filesize {' . PHP_EOL;
	echo '	color: #333;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filebase {' . PHP_EOL;
	echo '	color: #777777;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filedate{' . PHP_EOL;
	echo '	color: #333;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#filetime{' . PHP_EOL;
	echo '	color: #333;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#status {' . PHP_EOL;
	echo '	font-size: small;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#statustext {' . PHP_EOL;
	echo '	color: #555555;' . PHP_EOL;
	echo '	font-style: italic;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#statusdate {' . PHP_EOL;
	echo '	font-weight: bolder;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#statustime {' . PHP_EOL;
	echo '	font-weight: bolder;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#freespace {' . PHP_EOL;
	echo '	font-style: italic;' . PHP_EOL;
	echo '	color: #333;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#freesize {' . PHP_EOL;
	echo '	font-weight: bolder;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#freetext {' . PHP_EOL;
	echo '	color: #777777;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo '#freebase {' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo PHP_EOL;
	echo '#nofiles {' . PHP_EOL;
	echo '	display: block;' . PHP_EOL;
	echo '	margin: 15pt 15pt 10pt 30pt;' . PHP_EOL;
	echo '	color: #A00000;' . PHP_EOL;
	echo '	font-style: italic;' . PHP_EOL;
	echo '	font-weight: bolder;' . PHP_EOL;
	echo '}' . PHP_EOL;
	echo PHP_EOL;

	unset ($machine);
}

function Main () {

	date_default_timezone_set('America/New_York');
	if (is_link('/etc/localtime')) {
		$filename = readlink('/etc/localtime');
		if (strpos($filename, '/usr/share/zoneinfo/') === 0) {
    			$timezone = substr($filename, 20);
			date_default_timezone_set($timezone);
	 	}
	}

	if ( $_GET['css'] == '' ) {
		printHTMLPage ();
	} else  {
		printCSSPage ();
	}
}


Main ();

?>
